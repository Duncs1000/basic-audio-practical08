//
//  Counter.cpp
//  JuceBasicAudio
//
//  Created by Duncan Whale on 11/17/14.
//
//

#include "Counter.h"

// Constructor.
Counter::Counter()  :   Thread ("CounterThread"),
                        listener (nullptr)
{
    resetCounter();
    pause = 200;
}

// Destructor.
Counter::~Counter()
{
    // Stop the 'Counter' thread.
    if (isThreadRunning())
        stopThread(500);
}

void Counter::startCounter()
{
    startThread();
}

void Counter::stopCounter()
{
    stopThread(500);
    resetCounter();
}

void Counter::resetCounter()
{
    counter = 0;
}

void Counter::setPauseTime(uint32 time)
{
    pause = time;
}

bool Counter::isCounterRunning()
{
    if (isThreadRunning())
        return true;
    else
        return false;
}

void Counter::run()
{
    while (!threadShouldExit())
    {
        uint32 time = Time::getMillisecondCounter();
        
        if (listener != nullptr)
            listener->counterChanged (counter++);
        
        Time::waitForMillisecondCounter (time + pause);
    }
}

void Counter::setListener (Listener* newListener)
{
    listener = newListener;
}