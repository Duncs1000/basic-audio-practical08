/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"


//==============================================================================
MainComponent::MainComponent (Audio& audio_) : audio (audio_)
{
    // Set the window size.
    setSize (500, 400);
    
    // Set up gain slider.
    gainSlider.setSliderStyle(juce::Slider::LinearVertical);
    gainSlider.setRange(0.0, 1.0);
    gainSlider.setValue(0.5);
    gainSlider.addListener(this);
    addAndMakeVisible(&gainSlider);
    
    // Set up metronome gain slider.
    metroGainSlider.setSliderStyle(juce::Slider::LinearVertical);
    metroGainSlider.setRange(0.0, 1.0);
    metroGainSlider.setValue(0.5);
    metroGainSlider.addListener(this);
    addAndMakeVisible(&metroGainSlider);
    
    // Set up BPM slider.
    bpmSlider.setSliderStyle(juce::Slider::IncDecButtons);
    bpmSlider.setRange(40, 400, 1);
    bpmSlider.setValue(120);
    bpmSlider.addListener(this);
    addAndMakeVisible(&bpmSlider);
    
    // Set up cycle duty slider.
    dutyCycleSlider.setSliderStyle(juce::Slider::LinearHorizontal);
    dutyCycleSlider.setRange(0.0, 1.0);
    dutyCycleSlider.setValue(0.5);
    dutyCycleSlider.addListener(this);
    addAndMakeVisible(dutyCycleSlider);
    dutyCycleSlider.setVisible(false);
    
    // Set up labels.
    gainLabel.setText("Gain", dontSendNotification);
    metroGainLabel.setText("Metronome Gain", dontSendNotification);
    dutyCycleLabel.setText("Cycle Duty", dontSendNotification);
    addAndMakeVisible(&gainLabel);
    addAndMakeVisible(&metroGainLabel);
    addAndMakeVisible(&dutyCycleLabel);
    dutyCycleLabel.setVisible(false);
    
    // Set up start/stop counter button.
    counterStartStopButton.setButtonText("Start Metro");
    counterStartStopButton.addListener(this);
    addAndMakeVisible(&counterStartStopButton);
    
    // Set up waveform combobox.
    waveformCombobox.addItem("Sine", 1);
    waveformCombobox.addItem("Square", 2);
    waveformCombobox.addItem("Sawtooth", 3);
    waveformCombobox.addItem("Triangle", 4);
    waveformCombobox.addListener(this);
    addAndMakeVisible(&waveformCombobox);
    
    // Set up counter.
    counter.setListener(this);
    counter.resetCounter();
}

MainComponent::~MainComponent()
{
    // Stop the counter.
    if (counter.isCounterRunning())
        counter.stopCounter();
}

void MainComponent::resized()
{
    // First column.
    gainLabel.setBounds(10, 10, 100, 40);
    gainSlider.setBounds(10, 60, 100, getHeight() - 80);
    
    // Middle column.
    counterStartStopButton.setBounds(120, 10, getWidth() / 2.0, 40);
    bpmSlider.setBounds(120, 60, getWidth() / 2.0, 40);
    waveformCombobox.setBounds(120, 110, getWidth() / 2.0, 40);
    dutyCycleLabel.setBounds(120, 160, getWidth() / 8.0, 40);
    dutyCycleSlider.setBounds(120 + (getWidth() / 8.0), 160, (getWidth() / 8.0) * 3.0, 40);
    
    // Third column.
    metroGainLabel.setBounds((getWidth() / 2.0) + 130, 10, 100, 40);
    metroGainSlider.setBounds((getWidth() / 2.0) + 130, 60, 100, getHeight() - 80);
}

void MainComponent::sliderValueChanged (Slider* slider)
{
    // If slider is gain slider, update gain in audio.
    if (&gainSlider == slider)
    {
        DBG("Gain has changed. New gain: " << slider->getValue() << ".\n");
        sharedMemory.enter();
        audio.audioSetAmplitude(slider->getValue() * slider->getValue() * slider->getValue());
        sharedMemory.exit();
    }
    else if (&bpmSlider == slider)
    {
        DBG("BPM has changed. New BPM: " << slider->getValue() << ".\n");
        counter.setPauseTime((uint16)(60000 / slider->getValue()));
    }
    else if (&metroGainSlider == slider)
    {
        DBG("Metronome gain has changed. New gain: " << slider->getValue() << ".\n");
        sharedMemory.enter();
        audio.audioSetMetroAmplitude(slider->getValue() * slider->getValue() * slider->getValue());
        sharedMemory.exit();
    }
    else if (&dutyCycleSlider == slider)
    {
        DBG("Duty cycle has changed. New duty cycle: " << slider->getValue() << ".\n");
        sharedMemory.enter();
        audio.audioSetDutyCycle(slider->getValue());
        sharedMemory.exit();
    }
}

void MainComponent::buttonClicked (Button* button)
{
    // Counter button...
    if (button == &counterStartStopButton)
    {
        if (!counter.isCounterRunning())
        {
            // Start the counter.
            counter.startCounter();

            // Update the button.
            counterStartStopButton.setToggleState(true, dontSendNotification);
            counterStartStopButton.setButtonText("Stop Metro");
        }
        else
        {
            // Reset the counter and display a message on the console.
            counter.stopCounter();
            std::cout << "\n\t\t--The count has stopped.--\n\t\t--The counter has been reset.--\n\n";
            
            // Update the button.
            counterStartStopButton.setToggleState(false, dontSendNotification);
            counterStartStopButton.setButtonText("Start Metro");
        }
    }
}

void MainComponent::comboBoxChanged (ComboBox* comboBoxThatHasChanged)
{
    if (comboBoxThatHasChanged == &waveformCombobox)
    {
        // If sine wave has been chosen...
        if (comboBoxThatHasChanged->getSelectedId() == 1)
        {
            // Set the pointer to point to the sine oscillator, and remove the square wave duty cycle control from the UI.
            sharedMemory.enter();
            audio.audioSetWaveform("Sine");
            sharedMemory.exit();
            
            dutyCycleLabel.setVisible(false);
            dutyCycleSlider.setVisible(false);
        }
        // If square wave has been chosen...
        else if (comboBoxThatHasChanged->getSelectedId() == 2)
        {
            // Set the pointer to point to the square oscillator, and add the square wave duty cycle control to the UI.
            sharedMemory.enter();
            audio.audioSetWaveform("Square");
            sharedMemory.exit();
            
            dutyCycleLabel.setVisible(true);
            dutyCycleSlider.setVisible(true);
        }
        else if (comboBoxThatHasChanged->getSelectedId() == 3)
        {
            // Set the pointer to point to the sawtooth oscillator, and remove the square wave duty cycle control from the UI.
            sharedMemory.enter();
            audio.audioSetWaveform("Saw");
            sharedMemory.exit();
            
            dutyCycleLabel.setVisible(false);
            dutyCycleSlider.setVisible(false);
        }
        else if (comboBoxThatHasChanged->getSelectedId() == 4)
        {
            // Set the pointer to point to the triangle oscillator, and remove the square wave duty cycle control from the UI.
            sharedMemory.enter();
            audio.audioSetWaveform("Triangle");
            sharedMemory.exit();
            
            dutyCycleLabel.setVisible(false);
            dutyCycleSlider.setVisible(false);
        }
    }
}

void MainComponent::counterChanged (const unsigned int counterValue)
{
    std::cout << "Counter: " << counterValue << "\n";
    audio.beep();
}

//MenuBarCallbacks==============================================================
StringArray MainComponent::getMenuBarNames()
{
    const char* const names[] = { "File", 0 };
    return StringArray (names);
}

PopupMenu MainComponent::getMenuForIndex (int topLevelMenuIndex, const String& menuName)
{
    PopupMenu menu;
    if (topLevelMenuIndex == 0)
        menu.addItem(AudioPrefs, "Audio Prefrences", true, false);
    return menu;
}

void MainComponent::menuItemSelected (int menuItemID, int topLevelMenuIndex)
{
    if (topLevelMenuIndex == FileMenu)
    {
        if (menuItemID == AudioPrefs)
        {
            AudioDeviceSelectorComponent audioSettingsComp (audio.getAudioDeviceManager(), 0, 2, 2, 2, true, true, true, false);
            audioSettingsComp.setSize (450, 350);
            DialogWindow::showModalDialog ("Audio Settings", &audioSettingsComp, this, Colours::lightgrey, true);
        }
    }
}

