//
//  Counter.h
//  JuceBasicAudio
//
//  Created by Duncan Whale on 11/17/14.
//
//

#ifndef COUNTER_H
#define COUNTER_H

#include "../../JuceLibraryCode/JuceHeader.h"
#include <iostream>

/**
 Class for a counter that utilises its own thread.
 */

class Counter : public Thread
{
public:
    /** Constructor. */
    Counter();
    
    /** Destructor. */
    ~Counter();
    
    /** Starts the counter. */
    void startCounter();
    
    /** Stops the counter. */
    void stopCounter();
    
    /** Sets the counter position to 0. */
    void resetCounter();
    
    /** Sets the time between information sent to the listener(s).
     @param time - time in milliseconds.
     */
    void setPauseTime(uint32 time);
    
    /** Returns true if the counter is running. */
    bool isCounterRunning();
    
    /** Handles the actual count (in its own thread). */
    void run() override;
    
    /** Class for counter listeners to inherit. */
    class Listener
    {
    public:
        /** Destructor. */
        virtual ~Listener() {}
        
        /** Called when the next timer has reached the next interval. */
        virtual void counterChanged (const unsigned int counterValue) = 0;
    };
    
    /** Sets the listener.
     @param setListener - pointer to the new listener.
     */
    void setListener (Listener* newListener);
    
private:
    uint32 counter;
    uint16 pause;
    Listener* listener;
};

#endif /* defined(COUNTER_H) */
