/*
  ==============================================================================

    Audio.h
    Created: 13 Nov 2014 8:14:40am
    Author:  Tom Mitchell

  ==============================================================================
*/

#ifndef AUDIO_H_INCLUDED
#define AUDIO_H_INCLUDED

/**
 Class containing all audio processes
 */

#include "../../JuceLibraryCode/JuceHeader.h"
#include "SinOscillator.h"
#include "SquareOscillator.h"
#include "SawOscillator.h"
#include "TriangleOscillator.h"

class Audio :   public MidiInputCallback,
                public AudioIODeviceCallback
{
public:
    /** Constructor. */
    Audio();
    
    /** Destructor. */
    ~Audio();
    
    /** Returns the audio device manager, don't keep a copy of it! */
    AudioDeviceManager& getAudioDeviceManager() { return audioDeviceManager;}
    
    /** Handles all MIDI data.
     @param message - contains all the MIDI information.
     */
    void handleIncomingMidiMessage (MidiInput* source, const MidiMessage& message) override;
    
    /** Handles the audio data. */
    void audioDeviceIOCallback (const float** inputChannelData, int numInputChannels, float** outputChannelData, int numOutputChannels, int numSamples) override;
    
    
    // Devices.
    /** Called when a device is about to start. */
    void audioDeviceAboutToStart (AudioIODevice* device) override;
    
    /** Called when a device stops. */
    void audioDeviceStopped() override;
    
    /** Sets the waveform for the synth.
     @param wave - the waveform that will be set.
     */
    void audioSetWaveform(String wave);
    
    
    /** Sets the duty cycle for SQUARE waves.
     @param cycle - the position of the midpoint, between 0.0 and 1.0.
     */
    void audioSetDutyCycle(float cycle);
    
    
    // Amplitudes, get and set.
    /** Sets the amplitude of the synth output. */
    void audioSetAmplitude(float value);
    
    /** Returns the amplitude of the synth output. */
    float audioGetAmplitude();
    
    /** Sets the amplitude of the metronome. */
    void audioSetMetroAmplitude(float value);
    
    /** Returns the amplitude of the metronome. */
    float audioGetMetroAmplitude();
    
    /** Returns the current sample rate. */
    int audioGetSampleRate() const;
    
    /** Emits a short sine 'beep' for the metronome. */
    void beep();
private:
    AudioDeviceManager audioDeviceManager;
    CriticalSection sharedMemory;
    
    // Oscillators.
    Oscillator *oscillatorPointer;
    SinOscillator sinOscillator;
    SquareOscillator squareOscillator;
    SawOscillator sawOscillator;
    TriangleOscillator triangleOscillator;
    SinOscillator beepOscillator;
    
    float amplitude;
    float metroAmplitude;
    float sampleRate;
    const float twoPi = 2 * M_PI;
    
    float synthOutLeft;
    float synthOutRight;
    float mix;
    float metro;
};



#endif  // AUDIO_H_INCLUDED
