/*
  ==============================================================================

    Audio.cpp
    Created: 13 Nov 2014 8:14:40am
    Author:  Tom Mitchell

  ==============================================================================
*/

#include "Audio.h"

Audio::Audio()
{
    // Set some defaults for the synth.
    synthOutLeft = 0.f;
    synthOutRight = 0.f;
    mix = 0.f;
    metro = 0.f;
    
    oscillatorPointer = &sinOscillator;
    oscillatorPointer->setAmplitude(0.f);
    oscillatorPointer->setFrequency(440.f);
    
    // Set up the audio device manager.
    audioDeviceManager.initialiseWithDefaultDevices (2, 2); //2 inputs, 2 outputs
    
    audioDeviceManager.addMidiInputCallback (String::empty, this);
    audioDeviceManager.addAudioCallback (this);

}

Audio::~Audio()
{
    audioDeviceManager.removeAudioCallback (this);
    audioDeviceManager.removeMidiInputCallback (String::empty, this);
}


void Audio::handleIncomingMidiMessage (MidiInput* source, const MidiMessage& message)
{
    // All MIDI inputs arrive here
    
    // Note on!
    if (message.isNoteOn())
    {
        sharedMemory.enter();
        oscillatorPointer->setFrequency(MidiMessage::getMidiNoteInHertz(message.getNoteNumber()));
        oscillatorPointer->setAmplitude(amplitude);
        sharedMemory.exit();
    }
    // Note off!
    else if (message.isNoteOff() && (float)message.getMidiNoteInHertz(message.getNoteNumber()) == oscillatorPointer->getFrequency())
    {
        sharedMemory.enter();
        oscillatorPointer->setAmplitude(0.0);
        sharedMemory.exit();
    }
}

void Audio::audioDeviceIOCallback (const float** inputChannelData,
                                           int numInputChannels,
                                           float** outputChannelData,
                                           int numOutputChannels,
                                           int numSamples)
{
    //All audio processing is done here
    const float *inL = inputChannelData[0];
    const float *inR = inputChannelData[1];
    float *outL = outputChannelData[0];
    float *outR = outputChannelData[1];
    
    while(numSamples--)
    {
        if (oscillatorPointer != nullptr)
        {
            sharedMemory.enter();
            mix = oscillatorPointer->nextSample();
            sharedMemory.exit();
        }
        
        sharedMemory.enter();
        metro = beepOscillator.nextSample();
        sharedMemory.exit();
        
        synthOutLeft = synthOutRight = mix;
        
        *outL = metro + synthOutLeft;
        *outR = metro + synthOutRight;
        
        inL++;
        inR++;
        outL++;
        outR++;
    }
}


void Audio::audioDeviceAboutToStart (AudioIODevice* device)
{
    sampleRate = device->getCurrentSampleRate();
}

void Audio::audioDeviceStopped()
{

}

void Audio::audioSetWaveform(String wave)
{
    if (wave == "Sine")
        oscillatorPointer = &sinOscillator;
    else if (wave == "Square")
        oscillatorPointer = &squareOscillator;
    else if (wave == "Saw")
        oscillatorPointer = &sawOscillator;
    else if (wave == "Triangle")
        oscillatorPointer = &triangleOscillator;
    
    oscillatorPointer->setAmplitude(0.f);
    oscillatorPointer->setFrequency(0.f);
}

void Audio::audioSetDutyCycle(float cycle)
{
    squareOscillator.setDutyCycle(cycle);
}

void Audio::audioSetAmplitude(float value)
{
    sharedMemory.enter();
    amplitude = value;
    sharedMemory.exit();
}

float Audio::audioGetAmplitude()
{
    sharedMemory.enter();
    return amplitude;
    sharedMemory.exit();
}

void Audio::audioSetMetroAmplitude(float value)
{
    sharedMemory.enter();
    metroAmplitude = value;
    sharedMemory.exit();
}

float Audio::audioGetMetroAmplitude()
{
    sharedMemory.enter();
    return metroAmplitude;
    sharedMemory.exit();
}

int Audio::audioGetSampleRate() const
{
    return sampleRate;
}

void Audio::beep()
{
    uint32 time = Time::getMillisecondCounter();
    
    beepOscillator.setFrequency(440.f);
    beepOscillator.setAmplitude(0.5f * metroAmplitude);
    
    Time::waitForMillisecondCounter (time + 100);
    beepOscillator.setAmplitude(0.f);
}