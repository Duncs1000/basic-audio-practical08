/*
 *  SinOscillator.h
 *  sdaAudioMidi
 *
 *  Created by tjmitche on 11/11/2010.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */


#ifndef H_SINOSCILLATOR
#define H_SINOSCILLATOR

#include "../JuceLibraryCode/JuceHeader.h"
#include "Oscillator.h"

/**
 Class for a sinewave oscillator. @see Oscillator
 */

class SinOscillator : public Oscillator
{
public:
    /** Constructor */
    SinOscillator();
    
    /** Destructor */
    ~SinOscillator();
    
    /** Works out the shape of the wave, from the phase values.
     @param currentPhase - the current position in the phase cycle.
     @return - the resulting sample.
     */
    float renderWaveShape (const float currentPhase) override;
};

#endif //H_SINOSCILLATOR