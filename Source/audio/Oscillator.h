/*
 *  Oscillator.h
 *  sdaAudioMidi
 *
 *  Created by tjmitche on 11/11/2010.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */


#ifndef H_OSCILLATOR
#define H_OSCILLATOR

#include "../JuceLibraryCode/JuceHeader.h"

/**
 Abstract base class for a generic oscillator.
 */

class Oscillator
{
public:
	//==============================================================================
	/**
	 Oscillator constructor
	 */
	Oscillator();
	
	/**
	 Oscillator destructor
	 */
	virtual ~Oscillator();
	
	/**
	 Sets the frequency of the oscillator.
	 */
	void setFrequency (float freq);
    
    /**
     Gets the frequency of the oscillator.
     */
    float getFrequency() const;
	
	/**
	 Sets frequency using a midi note number.
	 */
	void setNote (int noteNum);
	
	/**
	 Sets the amplitude of the oscillator.
	 */
	void setAmplitude (float amp);
	
	/**
	 Resets the oscillator.
	 */
	void reset();
	
	/**
	 Sets the sample rate.
	 */
	void setSampleRate (float sr);
	
	/**
	 Returns the next sample.
	 */
	float nextSample();
	
	/**
	 Function that provides the execution of the waveshape.
	 */
	virtual float renderWaveShape (const float currentPhase) = 0;
	
private:
	float frequency;
    float amplitude;
    float sampleRate;
	float phase;
    float phaseInc;
};

#endif //H_OSCILLATOR