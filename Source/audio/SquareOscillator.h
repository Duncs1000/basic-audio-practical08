//
//  SquareOscillator.h
//  JuceBasicAudio
//
//  Created by Duncan Whale on 11/20/14.
//
//

#ifndef SQUAREOSCILLATOR_H
#define SQUAREOSCILLATOR_H

#include <iostream>
#include "Oscillator.h"

/**
 Class for a squarewave oscillator. @see Oscillator
 */

class SquareOscillator : public Oscillator
{
public:
    /** Constructor */
    SquareOscillator();
    
    /** Destructor */
    ~SquareOscillator();
    
    /** Works out the shape of the wave, from the phase values.
     @param currentPhase - the current position in the phase cycle.
     @return - the resulting sample.
     */
    float renderWaveShape (const float currentPhase) override;
    
    /** Sets the mid-point of the square wave.
     @param cycleControl - the midpoint of the wave, from 0.0 to 1.0.
     */
    void setDutyCycle (const float cycleControl);
private:
    float dutyCycle;
};

#endif /* defined(SQUAREOSCILLATOR_H) */
