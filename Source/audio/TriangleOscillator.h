/*
 *  TriangleOscillator.h
 *  sdaAudioMidi
 *
 *  Created by Duncan Whale on 24/11/2014.
 *  Copyright 2014 __MyCompanyName__. All rights reserved.
 *
 */


#ifndef H_TRIANGLEOSCILLATOR
#define H_TRIANGLEOSCILLATOR

#include "../JuceLibraryCode/JuceHeader.h"
#include "Oscillator.h"

/**
 Class for a triangle wave oscillator. @see Oscillator
 */

class TriangleOscillator : public Oscillator
{
public:
    /** Constructor */
    TriangleOscillator();
    
    /** Destructor */
    ~TriangleOscillator();
    
    /** Works out the shape of the wave, from the phase values.
     @param currentPhase - the current position in the phase cycle.
     @return - the resulting sample.
     */
    float renderWaveShape (const float currentPhase) override;
};

#endif //H_TRIANGLEOSCILLATOR