/*
 *  SawOscillator.cpp
 *  sdaAudioMidi
 *
 *  Created by Duncan Whale on 24/11/2014.
 *  Copyright 2014 __MyCompanyName__. All rights reserved.
 *
 */

#include "SawOscillator.h"
#include <cmath>

SawOscillator::SawOscillator()
{
    
}

SawOscillator::~SawOscillator()
{
    
}

float SawOscillator::renderWaveShape (const float currentPhase)
{
    return (currentPhase / M_PI) - 1.0;
}