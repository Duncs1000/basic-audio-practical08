//
//  SquareOscillator.cpp
//  JuceBasicAudio
//
//  Created by Duncan Whale on 11/20/14.
//
//

#include "SquareOscillator.h"

SquareOscillator::SquareOscillator()
{
    dutyCycle = M_PI;
}

SquareOscillator::~SquareOscillator()
{
    
}

float SquareOscillator::renderWaveShape (const float currentPhase)
{
    float out;
    
    // Square wave calculation.
    if (currentPhase <= dutyCycle)
        out = 1;
    else if (currentPhase <= 2 * M_PI)
        out = -1;
    else
        out = 0;
    
    return out;
}

void SquareOscillator::setDutyCycle (const float cycleControl)
{
    // Set a new mid-point for the square wave.
    if (cycleControl >= 0 && cycleControl <= 1.0)
        dutyCycle = (2 * M_PI * cycleControl);
    else
    {
        std::cout << "An invalid value of 'duty cycle control' has been passed in. The value must be between 0 and 1.\n";
        dutyCycle = M_PI;
    }
}