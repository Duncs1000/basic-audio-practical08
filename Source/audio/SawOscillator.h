/*
 *  SawOscillator.h
 *  sdaAudioMidi
 *
 *  Created by Duncan Whale on 24/11/2014.
 *  Copyright 2014 __MyCompanyName__. All rights reserved.
 *
 */


#ifndef H_SAWOSCILLATOR
#define H_SAWOSCILLATOR

#include "../JuceLibraryCode/JuceHeader.h"
#include "Oscillator.h"

/**
 Class for a sawtooth oscillator. @see Oscillator
 */

class SawOscillator : public Oscillator
{
public:
    /** Constructor */
    SawOscillator();
    
    /** Destructor */
    ~SawOscillator();
    
    /** Works out the shape of the wave, from the phase values.
     @param currentPhase - the current position in the phase cycle.
     @return - the resulting sample.
     */
    float renderWaveShape (const float currentPhase) override;
};

#endif //H_SAWOSCILLATOR