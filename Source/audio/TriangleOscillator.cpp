/*
 *  TriangleOscillator.cpp
 *  sdaAudioMidi
 *
 *  Created by Duncan Whale on 24/11/2014.
 *  Copyright 2014 __MyCompanyName__. All rights reserved.
 *
 */

#include "TriangleOscillator.h"
#include <cmath>

TriangleOscillator::TriangleOscillator()
{
    
}

TriangleOscillator::~TriangleOscillator()
{
    
}

float TriangleOscillator::renderWaveShape (const float currentPhase)
{
    if (currentPhase <= M_PI)
        return ((currentPhase / M_PI) * 2.0) - 1.0;
    else
        return ((currentPhase / M_PI) * -2.0) + 3.0;
}